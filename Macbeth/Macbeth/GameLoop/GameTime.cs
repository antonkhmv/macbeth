﻿namespace Macbeth
{
    public class GameTime
    {
        private float deltatime = 0f;
        public float Scale { get; private set; } = 0f;
        public float DeltaTime { get => deltatime*Scale; private set => deltatime = value; }
        public float TotalTimeElapsed { get; private set; } = 0f;
        public void Update(float delta, float total, float scale)
        {
            deltatime = delta;
            Scale = scale;
            TotalTimeElapsed = total;
        }
    }
}
