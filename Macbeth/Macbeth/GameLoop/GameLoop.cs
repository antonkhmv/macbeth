﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;

namespace Macbeth
{
    public abstract class GameLoop
    {
        private static float fps = 60.0f;
        private static float updateTime = 1f / FPS;
        public RenderWindow Window { get; protected set; }
        public string WindowTitle { get; protected set; }
        public GameTime GameTime { get; protected set; }
        protected static float scale = 1F;

        public static float FPS
        {
            get => fps; set
            {
                fps = value;
                updateTime = 1f / value;
            }
        }
        public static float UpdateTime { get => updateTime; }

        protected GameLoop(uint width, uint height, string title)
        {
            this.Window = new RenderWindow(new VideoMode(width, height), title);
            this.GameTime = new GameTime();
            this.Window.Closed += Window_Closed;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Window.Close();
        }

        public void Run()
        {
            LoadContent();
            Initialize();

            var cloak = new Clock();

            float totalTimeElapsed;
            float previousTimeElapsed = 0;
            float deltaTime;

            float timeSinceLastUpdate = 0f;
            

            while(Window.IsOpen)
            {
                Window.DispatchEvents();
                totalTimeElapsed = cloak.ElapsedTime.AsSeconds();
                deltaTime = totalTimeElapsed - previousTimeElapsed;
                previousTimeElapsed = totalTimeElapsed;
                timeSinceLastUpdate += deltaTime;

                if (timeSinceLastUpdate >= updateTime * scale) {
                    GameTime.Update(timeSinceLastUpdate, totalTimeElapsed, 1/scale);
                    timeSinceLastUpdate = 0f;
                    Render();
                    Update();
                    Window.Display();
                    Window.Clear(new Color(175, 176, 176));
                }
            }
        }

        public abstract void LoadContent();
        public abstract void Initialize();
        public abstract void Update();
        public abstract void Render();

    }

}
