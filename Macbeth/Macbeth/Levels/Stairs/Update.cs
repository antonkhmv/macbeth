﻿using Collision;
using SFML.Graphics;
using SFML.System;
using System.Collections.Generic;

namespace Macbeth.Levels
{
    

    public partial class Stairs : GameLoop
    {
        /// <summary>
        /// Which stairs are now walkable
        /// false = lower, true = upper.
        /// </summary>
        static bool stairs = true;

        /// <summary>
        /// The acceleration of gravity
        /// </summary>
        const float g = 980f;

        /// <summary>
        /// Cooldown for stair change to make life easier
        /// </summary>
        static float stairChangeCoolDown = 0f;

        static int coolDown = 30;

        static int count = -1;
        static int countdown = 0;

        public override void Update()
        {
            
            macbeth.dy += g * GameTime.DeltaTime;

            if (macbeth.dx != 0)
            {
                macbeth.sprite.Position += new Vector2f(macbeth.dx * GameTime.DeltaTime, 0);
            }

            #region Wall & Platform Collisions
            var collitions = new List<Drawable>() { leftWall, rightWall };
            if (stairs)
            {
                collitions.Add(pltUpperStairs);
            }
            else
            {
                collitions.Add(pltLowerStairs);
            }

            bool changeStairs = false;

            foreach (var item in collitions) {
                Vector2f offset = new Vector2f(0,0);
                CollisionTester.Collision coll = CollisionTester.Collision.No;
                if (item.GetType() == typeof(Platform))
                {
                    coll = CollisionTester.DetectCollition(item as Platform, macbeth, out offset);
                }
                else if (item.GetType() == typeof(RectangleShape)){
                    coll = CollisionTester.DetectCollition(item as RectangleShape, macbeth, out offset);
                }
                if (coll != CollisionTester.Collision.No)
                {
                    macbeth.sprite.Position += offset;
                    if (coll == CollisionTester.Collision.Vert)
                    {
                        macbeth.dy = 0;
                    }
                    if (coll == CollisionTester.Collision.Hor)
                    {
                        macbeth.dx = 0;
                    }

                    if (item == rightWall)
                    {
                        changeStairs = true;
                    }
                }
            }
            #endregion
            #region Handling Movment
            if (GameTime.TotalTimeElapsed - stairChangeCoolDown > coolDown * UpdateTime && changeStairs)
            {
                stairs = !stairs;
                stairChangeCoolDown = GameTime.TotalTimeElapsed;
            }

            if (macbeth.dy != 0)
            {
                macbeth.sprite.Position += new Vector2f(0, macbeth.dy * GameTime.DeltaTime);
            }
            if (macbeth.dx != 0)
            {
                macbeth.SetFrame(1 + count);
                countdown++;
                if (countdown == 4)
                {
                    count = (count + 1) % 10;
                    countdown = 0;
                }
            }
            else if (macbeth.dx == 0 && count != -1)
            {
                count = -1;
                countdown = 0;
                macbeth.SetFrame(1 + count);
            }
            #endregion

        }
    }
}
