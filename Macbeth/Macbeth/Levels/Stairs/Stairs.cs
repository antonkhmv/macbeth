﻿using SFML.Graphics;

namespace Macbeth.Levels
{
    public partial class Stairs : GameLoop
    {
        // the scene has a set resolution at 730 x 445 so it's scalable.
        private static float windowScale = 1.7F;
        private static uint windowWidth = (uint)(730 * windowScale);
        private static uint windowHeight = (uint)(445 * windowScale);
        
        private static string windowTitle = "Macbeth";
        private static Font pixelFont;

        static int totalSum = 0;
        static int timesAnswered = 0;
        static bool showDialogue = false;
        static bool showFrogsAnswer = false;
        static bool showDebugData = false;
        static float radius = 23f * windowScale;

        public Stairs() : base(windowWidth, windowHeight, windowTitle) {
            scale = 1F;
        }
    }
}
