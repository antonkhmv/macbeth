﻿using SFML.Graphics;
using SFML.System;
using System.Collections.Generic;

namespace Macbeth.Levels
{
    public partial class Stairs : GameLoop
    {
        // Platform
        static Platform pltUpperStairs;
        static Platform pltLowerStairs;

        static RectangleShape rightWall;
        static RectangleShape leftWall;

        static NPC npcFrog;
        static NPC npcBanqo;
        static NPC npcLadyMacbeth;
        static NPC npcKing;

        static Player macbeth;

        public override void Initialize()
        {
            #region Events
            Window.KeyPressed += OnKeyDown;
            Window.KeyReleased += OnKeyUp;
            Window.MouseButtonPressed += OnMouseDown;
            #endregion
            #region Characters
            npcFrog = new NPC(frog, Box(new Vector2i(74, 130), new Vector2i(104, 165)), windowScale);
            npcBanqo = new NPC(banqo, Box(new Vector2i(110, 239), new Vector2i(135, 284)), windowScale);
            npcLadyMacbeth = new NPC(ladymacbeth, Box(new Vector2i(412, 119), new Vector2i(440, 165)), windowScale);
            npcKing = new NPC(king, Box(new Vector2i(365, 233), new Vector2i(383, 286)), windowScale);

            macbeth = new Player(_macbeth, Box(new Vector2i(110, 116), new Vector2i(138, 165)),
                new List<Vector2i>() {
                new Vector2i(6, 34), new Vector2i(35,70), new Vector2i(71,105), new Vector2i(106,148),
                new Vector2i(149,196), new Vector2i(197,243),new Vector2i(244,283),new Vector2i(284,317),
                new Vector2i(318,359), new Vector2i(360,404),new Vector2i(405,443),  }
                , windowScale);
            #endregion
            #region Platforms & Walls
            pltUpperStairs = new Platform(upperStairs, Box(new Vector2i(46, 165), new Vector2i(661, 291)),
                windowScale);

            pltLowerStairs = new Platform(lowerStairs, Box(new Vector2i(46, 219), new Vector2i(661, 291)),
                windowScale);

            leftWall = new RectangleShape(new Vector2f(47, windowHeight))
            { Scale = new Vector2f(windowScale, windowScale), FillColor = new Color(7, 7, 7) };

            rightWall = new RectangleShape(new Vector2f(48, windowHeight))
            { Scale = new Vector2f(windowScale, windowScale), FillColor = new Color(7, 7, 7), Position = new Vector2f(682, 0) * windowScale };

            #endregion
        }
    }
}
