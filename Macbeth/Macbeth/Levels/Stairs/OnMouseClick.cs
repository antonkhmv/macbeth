﻿using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Macbeth.Levels
{
    public partial class Stairs : GameLoop
    {
        public void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            #region Clicking Boxes
            if (e.Button == Mouse.Button.Left && showDialogue)
            {
                var boxes = new List<IntRect>() {
                    topLeftBox,
                    topRightBox,
                    bottomLeftBox,
                    bottomRightBox
                };

                for (int i = 0; i < 4; i++)
                {
                    if (ScaledBox((FloatRect)boxes[i]).Contains(e.X, e.Y))
                    {
                        totalSum += i + 1;
                        timesAnswered++;
                        showDialogue = false;
                    }
                }
            }
            #endregion
        }
    }
}
