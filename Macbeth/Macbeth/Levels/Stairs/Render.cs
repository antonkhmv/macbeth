﻿using SFML.Graphics;
using SFML.System;
using System.Collections.Generic;

namespace Macbeth.Levels
{
    public partial class Stairs : GameLoop
    {
        #region Texture Functions

        private Sprite FullScreenTexture(Texture texture)
        {
            return new Sprite(texture,
                new IntRect(new Vector2i(0, 0), new Vector2i((int)windowWidth, (int)windowHeight)))
            { Scale = new Vector2f(windowScale, windowScale) };
        }

        #endregion
        #region Text Functions
        private Text BoxedText(string contet, Font font, uint size, IntRect box, Color color)
        {
            Text text = new Text(contet, font) { CharacterSize = size, FillColor = color };
            var bounds = text.GetLocalBounds();
            box = new IntRect((Vector2i)(new Vector2f(box.Left, box.Top) * windowScale), (Vector2i)(new Vector2f(box.Width, box.Height) * windowScale));
            float x = box.Left + box.Width / 2f - bounds.Width / 2f;
            float y = box.Top + box.Height / 2f - bounds.Height / 2f - bounds.Top;
            text.Position = new Vector2f(x, y);
            return text;
        }
        private IntRect Box(Vector2i p1, Vector2i p2)
        {
            return new IntRect(p1.X, p1.Y, p2.X - p1.X, p2.Y - p1.Y);
        }
        #endregion

        static IntRect textBox;

        static IntRect topLeftBox;
        static IntRect topRightBox;
        static IntRect bottomRightBox;
        static IntRect bottomLeftBox;

        public FloatRect ScaledBox(FloatRect box)
        {
            return new FloatRect((new Vector2f(box.Left, box.Top) * windowScale), (new Vector2f(box.Width, box.Height) * windowScale));
        }

        public override void Render()
        {
            #region Textures 
            Window.Draw(FullScreenTexture(background));
            Window.Draw(FullScreenTexture(border));
            if (showDialogue)
            {
                Window.Draw(FullScreenTexture(answers));
                Window.Draw(FullScreenTexture(text));
            }
            Window.Draw(rightWall);
            Window.Draw(leftWall);

            Window.Draw(pltUpperStairs);
            Window.Draw(pltLowerStairs);
            #endregion
            #region Text
            // Coordinates in the original 730 x 445 resolution

            int dx = 364 - 177 + 3, dy = 395 - 366 + 4;
            textBox = Box(new Vector2i(177, 299), new Vector2i(553, 360));
            topLeftBox = Box(new Vector2i(177, 366), new Vector2i(364, 395));
            topRightBox = Box(new Vector2i(177 + dx, 366), new Vector2i(364 + dx, 395));
            bottomLeftBox = Box(new Vector2i(177, 366 + dy), new Vector2i(364, 395 + dy));
            bottomRightBox = Box(new Vector2i(177 + dx, 366 + dy), new Vector2i(364 + dx, 395 + dy));

            var one = Box(new Vector2i(0, 0), new Vector2i(100, 20));
            var two = Box(new Vector2i(0, 20), new Vector2i(100, 40));
            var three = Box(new Vector2i(100, 0), new Vector2i(200, 20));
            var four = Box(new Vector2i(100, 20), new Vector2i(200, 40));

            var debugData = new Text[]
            {
                BoxedText("X: " + macbeth.Position.X.ToString("f2"), pixelFont, 25, one, Color.White),
                 BoxedText($"Score: {totalSum}", pixelFont, 25, two, Color.White),
                 BoxedText(stairs ? "Upper" : "Lower",
                    pixelFont, 25, three, Color.White),
                 BoxedText($"FPS: {(1 / GameTime.DeltaTime).ToString("f0")}", pixelFont, 25, four, Color.White),
            };

            var texts = new Text[]
            {
                 BoxedText("привет", pixelFont, 36,  textBox, Color.Black),
                 BoxedText("1", pixelFont, 36,  topLeftBox, Color.Black),
                 BoxedText("2", pixelFont, 36,  topRightBox, Color.Black),
                 BoxedText("3", pixelFont, 36,  bottomLeftBox, Color.Black),
                 BoxedText("4", pixelFont, 36,  bottomRightBox, Color.Black),
            };
            if (showDebugData)
            {
                foreach (var text in debugData)
                {
                    Window.Draw(text);
                }
            }

            if (showDialogue)
            {
                foreach (var text in texts)
                {
                    Window.Draw(text);
                }
            }
            if (showFrogsAnswer)
            {
                Window.Draw(FullScreenTexture(text));
                if (totalSum < 8)
                {
                    Window.Draw(BoxedText("ква", pixelFont, 36, textBox, Color.Black));
                }
                else
                {
                    Window.Draw(BoxedText("ква ква", pixelFont, 36, textBox, Color.Black));
                }
            }
            #endregion

            #region NPCss

            var npcs = new List<NPC>() { npcLadyMacbeth, npcKing, npcFrog, npcBanqo };

            foreach (var npc in npcs)
            {
                if (showDebugData)
                {
                    Window.Draw(toRect(Expand(npc.sprite.GetGlobalBounds(), radius), Color.Green));
                }
            }

            Window.Draw(npcFrog);
            Window.Draw(npcLadyMacbeth);
            Window.Draw(npcKing);
            Window.Draw(npcBanqo);

            FloatRect intersection;
            pltUpperStairs.sprite.GetGlobalBounds().Intersects(macbeth.sprite.GetGlobalBounds(), out intersection);


            //Window.Draw(toRect(intersection, Color.Green));
            //Window.Draw(toRect((FloatRect)Collision.CollisionTester.GetCollitionBox(pltUpperStairs, macbeth), Color.White));
            //Window.Draw(toRect((FloatRect)Collision.CollisionTester.GetCollitionBox(pltLowerStairs, macbeth), Color.White));

            Window.Draw(macbeth);
            #endregion
        }
        public static FloatRect Expand(FloatRect rect, float range)
        {
            return new FloatRect(rect.Left - range, rect.Top, rect.Width + 2 * range, rect.Height);
        }
        public static Sprite SpriteFromBox(Texture texture, IntRect rect, float scale)
        {
            var pos = new Vector2f(rect.Left, rect.Top);
            var size = new Vector2i(rect.Width, rect.Height);

            return new Sprite(texture)
            {
                Position = pos * scale,
                Scale = new Vector2f(scale, scale)
            };
        }

        public static RectangleShape toRect(FloatRect rect, Color cl)
        {
            return new RectangleShape(new Vector2f(rect.Width, rect.Height))
            { Position = new Vector2f(rect.Left, rect.Top), FillColor = cl };
        }

        public static RectangleShape get_rekt(NPC xxx)
        {
            return new RectangleShape(new Vector2f(xxx.sprite.TextureRect.Width, xxx.sprite.TextureRect.Height))
            {
                Position = xxx.sprite.Position,
                Scale = xxx.sprite.Scale,
                FillColor = Color.Green
            };
        }
    }
}
