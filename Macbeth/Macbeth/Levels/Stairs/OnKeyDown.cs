﻿using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Macbeth.Levels
{
    public partial class Stairs : GameLoop
    {
        public void OnKeyDown(object sender, KeyEventArgs e)
        {
            #region Handling Movment(WSAD)
            if (e.Code == Keyboard.Key.A)
            {
                macbeth.dx = -macbeth.xSpeed;
                if (macbeth.sprite.Scale.X > 0)
                {
                    macbeth.sprite.Position += macbeth.offset;
                    macbeth.sprite.Scale = new Vector2f(-macbeth.sprite.Scale.X, macbeth.sprite.Scale.Y);
                }
            }

            if (e.Code == Keyboard.Key.D)
            {
                macbeth.dx = macbeth.xSpeed;
                if (macbeth.sprite.Scale.X < 0)
                {
                    macbeth.sprite.Position -= macbeth.offset;
                    macbeth.sprite.Scale = new Vector2f(-macbeth.sprite.Scale.X, macbeth.sprite.Scale.Y);
                }
            }
            #endregion

            #region Handling Dialogue
            if (e.Code == Keyboard.Key.Space)
            {
                // NPCs w/ dialogue
                var npcs = new List<NPC>() { npcLadyMacbeth, npcKing, npcBanqo };
                if (!showDialogue)
                {
                    foreach (var npc in npcs)
                    {
                        if (Expand(npc.sprite.GetGlobalBounds(), radius).Intersects(macbeth.sprite.GetGlobalBounds()) 
                            && !npc.dialogueShown)
                        {
                            showDialogue = true;
                            npc.dialogueShown = true;
                        }
                    }
                }

                if (timesAnswered == 3)
                {
                    if (Expand(npcFrog.sprite.GetGlobalBounds(), radius).Intersects(macbeth.sprite.GetGlobalBounds()))
                    {
                        showFrogsAnswer = true;
                    }
                }
            }
            #endregion

            if(e.Code == Keyboard.Key.F3)
            {
                showDebugData = !showDebugData;
            }
        }
        public void OnKeyUp(object sender, KeyEventArgs e)
        {
            #region Handling Movment (WSAD)
            if (e.Code == Keyboard.Key.A || e.Code == Keyboard.Key.D)
            {
                if (e.Code == Keyboard.Key.A && macbeth.dx == -macbeth.xSpeed)
                {
                    macbeth.dx = 0f;
                }
                if (e.Code == Keyboard.Key.D && macbeth.dx == macbeth.xSpeed)
                {
                    macbeth.dx = 0f;
                }
                macbeth.SetFrame(0);
            }
            #endregion

        }
    }
}
