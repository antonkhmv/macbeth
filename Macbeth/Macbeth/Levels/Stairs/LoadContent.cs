﻿using SFML.Graphics;

namespace Macbeth.Levels
{
    public partial class Stairs : GameLoop
    {
        private static Texture background;
        private static Texture border;
        private static Texture lowerStairs;
        private static Texture upperStairs;

        private static Texture answers;
        private static Texture text;

        private static Texture frog;
        private static Texture banqo;
        private static Texture king;
        private static Texture ladymacbeth;

        private static Texture _macbeth;

        public override void LoadContent()
        {
            string resourcePath = "../../Resources";
            // Checks for files locations for both release and debug configurations 
            // - not the best overall, but easy to mangage
            again:
            try
            {
                pixelFont = new Font(resourcePath + "/Fonts/18959.ttf");

                background = new Texture(resourcePath + "/Images/1.png");
                border = new Texture(resourcePath + "/Images/3.png");
                upperStairs = new Texture(resourcePath + "/Images/2_.png");
                lowerStairs = new Texture(resourcePath + "/Images/3_.png");

                answers = new Texture(resourcePath + "/Images/ответы.png");
                text = new Texture(resourcePath + "/Images/текст.png");

                frog = new Texture(resourcePath + "/Images/ляг_.png");
                banqo = new Texture(resourcePath + "/Images/б_.png");
                king = new Texture(resourcePath + "/Images/к_.png");
                ladymacbeth = new Texture(resourcePath + "/Images/лм_.png");

                _macbeth = new Texture(resourcePath + "/Images/MACBETH.png");
            }
            catch
            {
                if (resourcePath != "Resources")
                {
                    resourcePath = "Resources";
                    goto again;
                }
                Window.Close();
            }
        }
    }
}
