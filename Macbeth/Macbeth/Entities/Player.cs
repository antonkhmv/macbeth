﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Macbeth
{
    public class Player : GameObj
    {
        public float stepSize = 40;
        public int frame = 0;
        private List<Vector2i> positions;

        public int MaxFrames;

        public Vector2f offset;

        public Vector2f relPos = new Vector2f(0, 0);

        public RectangleShape HitBox
        {
            get {
                return new RectangleShape(new Vector2f(sprite.TextureRect.Width, sprite.TextureRect.Height))
                {
                    Position = sprite.Position,
                    Scale = sprite.Scale,
                    FillColor = Color.Green
                };
            }
        }
        /// <summary>
        /// Player's x-axis movment speed.
        /// </summary>
        public float xSpeed = 500f;

        /// <summary>
        /// Player's x-axis apparent velocity.
        /// </summary>
        public float dx = 0f;

        /// <summary>
        /// Player's y-axis apparent velocity.
        /// </summary>
        public float dy = 0f;

        /// <summary>
        /// Playable character with running animation 
        /// </summary>
        /// <param name="texture"></param>
        /// <param name="rect"></param>
        /// <param name="width">Single frame width.</param>
        /// <param name="scale"></param>
        public Player(Texture texture, IntRect rect, List<Vector2i> positions, float scale) : base()
        {
            var pos = new Vector2f(rect.Left, rect.Top);
            this.positions = positions;
            var size = new Vector2i((int)positions[0].Y + 1, rect.Height);
            MaxFrames = positions.Count;
            sprite = new Sprite(texture, new IntRect(positions[0].X, 0, positions[0].Y-positions[0].X, rect.Height))
            {
                Position = pos * scale,
                Scale = new Vector2f(scale, scale)
            };

            Size = size;
            offset = new Vector2f(size.Y, 0);
        }

        public void SetFrame(int frame)
        {
            sprite.TextureRect = new IntRect(positions[frame].X, 0, positions[frame].Y - positions[frame].X, sprite.TextureRect.Height);
        }
    }
}
