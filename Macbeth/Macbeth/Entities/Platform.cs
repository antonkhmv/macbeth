﻿using Collision;
using SFML.Graphics;

namespace Macbeth
{
    public class Platform : GameObj
    {

        public Platform(Texture texture, IntRect rect, float scale) : base(texture, rect, scale)
        {
        }
        
    }
}
