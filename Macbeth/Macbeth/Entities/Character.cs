﻿using SFML.Graphics;
using SFML.System;

namespace Macbeth
{

    public partial class GameObj : Drawable
    {
        public Sprite sprite;
        public Vector2f Position { get => sprite.Position; }
        public Vector2i Size { get; protected set; }

        public GameObj() { }

        public GameObj(Texture texture, IntRect rect, float scale)
        {
            var pos = new Vector2f(rect.Left, rect.Top);
            var size = new Vector2i(rect.Width, rect.Height);

            sprite = new Sprite(texture)
            {
                Position = pos * scale,
                Scale = new Vector2f(scale, scale)
            };
            Size = size;
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            target.Draw(sprite, states);
        }
    }
}
