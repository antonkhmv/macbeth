﻿using Collision;
using SFML.Graphics;
using SFML.System;

namespace Macbeth
{

    public partial class NPC : GameObj
    { 
        public bool dialogueShown = false;

        public NPC(Texture texture, IntRect rect, float scale) : base(texture, rect, scale)
        {
        }
    }
}
